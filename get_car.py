import time
from car_cache import Cache


class B:


    @classmethod
    def run(cls):
        while True:
            cls.worker()
            time.sleep(10)


    @classmethod
    def get_car(cls):
        while True:
            print("B : get_car_cache() -> ", Cache.get_car_cache())

    @classmethod
    def worker(cls):
        while True:
            cls.get_car()

class Cache:
    map = dict()

    @classmethod
    def set_car_cache(cls, car_name, price):
        print("set_car_cache ->", car_name, str(price))
        cls.map[car_name] = price

    @classmethod
    def get_car_cache(cls):
        return cls.map

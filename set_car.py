import time

from car_cache import Cache


class A:

    @classmethod
    def run(cls):
        while True:
            cls.worker()
            time.sleep(10)

    @classmethod
    def worker(cls):

        names_list = ["benze", "periad", "samand", "neysan", "new_car1", "new_car2", "new_car3", "new_car4", "new_car5",
                      "new_car6"]
        prices_list = [12, 15, 17, 26, 45, 68, 90, 12, 56, 19]

        for name, price in zip(names_list, prices_list):
            cls.set_car(name, price)
            time.sleep(3)

    @classmethod
    def set_car(cls, name, price):
        Cache.set_car_cache(name, price)
        print("A : set_car -> ", name, price)

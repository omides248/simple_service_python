import time
from threading import Thread

from get_car import B
from set_car import A

if __name__ == "__main__":
    Thread(target=A.run, args=(), daemon=True).start()
    Thread(target=B.run, args=(), daemon=True).start()
    time.sleep(1000)
